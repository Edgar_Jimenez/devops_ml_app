#!/bin/bash
# model_container
# Check if the container is running if not raise error
container_name=$1
if [ $(docker inspect -f '{{.State.Running}}' "$container_name") = "true" ]; then
  echo "container allready running" && docker stop "$container_name" && docker rm "$container_name";
  else echo "Container does not exist"; fi